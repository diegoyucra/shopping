package sample.stratio.shopping.unit;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import sample.stratio.shopping.domain.Currency;
import sample.stratio.shopping.services.ConvertService;
import sample.stratio.shopping.services.ConvertServiceImpl;

public class ConvertServiceTest {
	
	private ConvertService convertService;
	
	
	@Before
	public void before() {
		convertService = new ConvertServiceImpl();
	}
	
	@Test
	public void convertEur() {
		double value = 1d;
		double eurValue = convertService.toEur(value, Currency.Eur);
		assertEquals( 1 , eurValue, 0);
	}
	
	
	@Test
	public void convertPenny() {
		double value = 90d;
		double eurValue = convertService.toEur(value, Currency.Penny);
		assertEquals( 1, eurValue, 0.005d);
	}
	
	@Test
	public void convertPound() {
		double value = 0.9d;
		double eurValue = convertService.toEur(value, Currency.Pound);
		assertEquals( 1 , eurValue, 0.005d);
	}
	
}
