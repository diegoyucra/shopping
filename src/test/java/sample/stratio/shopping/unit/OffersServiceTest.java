package sample.stratio.shopping.unit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

import sample.stratio.shopping.domain.Basket;
import sample.stratio.shopping.domain.Goods;
import sample.stratio.shopping.domain.Offers;
import sample.stratio.shopping.services.ConvertService;
import sample.stratio.shopping.services.OffersService;
import sample.stratio.shopping.services.OffersServiceImpl;

public class OffersServiceTest {
	private OffersService offersService;
	
	@Before
	public void before() {
		offersService = new OffersServiceImpl();
		ConvertService mockConvertService = mock(ConvertService.class);
		when(mockConvertService.toEur(0, null)).thenReturn(1d);
		offersService.setConvertService( mockConvertService);
	}
	
	@Test
	public void checkApplesOffers() {
		Basket basket = new Basket(  new Goods[] {Goods.Apples});
		Offers offers = offersService.getOffers(basket);
		assertNotNull(offers);
		assertEquals( 1, offers.getOffers().size() );
		assertEquals( Goods.Apples, offers.getOffers().get(0).getGoods() );
	}
	
	
	@Test
	public void checkBreadOffers() {
		Basket basket = new Basket(  new Goods[] {Goods.Milk, Goods.Bread, Goods.Soup, Goods.Soup});
		Offers offers = offersService.getOffers(basket);
		assertNotNull(offers);
		assertEquals( 1, offers.getOffers().size() );
		assertEquals( Goods.Bread, offers.getOffers().get(0).getGoods() );
	}
	
	@Test
	public void checkNoOffers() {
		Basket basket = new Basket(  new Goods[] {Goods.Banana, Goods.Milk});
		Offers offers = offersService.getOffers(basket);
		assertNotNull(offers);
		assertEquals( 0, offers.getOffers().size() );
	}
}
