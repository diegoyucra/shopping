package sample.stratio.shopping.unit;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import sample.stratio.shopping.domain.Goods;
import sample.stratio.shopping.domain.Total;
import sample.stratio.shopping.services.PrintService;
import sample.stratio.shopping.services.PrintServiceImpl;

public class PrintServiceTest {
	private PrintService printService;
	
	
	@Before
	public void before() {
		printService = new PrintServiceImpl();
	}
	
	@Test
	public void printSubTotal() {
		Total total = new Total();
		total.setSubTotal( 25 );
		String result = printService.print(total);
		assertNotNull(result);
		assertTrue( result.contains("Subtotal:25.00 €"));
	}
	
	@Test
	public void printSubTotalNoOffers() {
		Total total = new Total();
		total.setSubTotal( 25 );
		String result = printService.print(total);
		assertNotNull(result);
		assertTrue( result.contains("Subtotal:25.00 € (No offers available)"));
	}
	
	@Test
	public void printSubTotalOffers() {
		Total total = new Total();
		total.setSubTotal( 25 );
		total.getOffers().addOffer(Goods.Apples, 10, 30);
		String result = printService.print(total);
		assertNotNull(result);
		assertTrue( result.contains("Subtotal:25.00 €"));
	}
	
	@Test
	public void printDiscount() {
		Total total = new Total();
		total.setSubTotal( 25 );
		total.getOffers().addOffer(Goods.Apples, 10, 30);
		total.getOffers().addOffer(Goods.Banana, 20, 40);
		String result = printService.print(total);
		assertNotNull(result);
		assertTrue( result.contains("\r\nApples 10% off: -30.00 €"));
		assertTrue( result.contains("\r\nBanana 20% off: -40.00 €"));
	}
	
	@Test
	public void printTotal() {
		Total total = new Total();
		total.setSubTotal( 25 );
		String result = printService.print(total);
		assertNotNull(result);
		assertTrue( result.contains("\r\nTotal:25.00 €"));
	}
}
