package sample.stratio.shopping.integration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import sample.stratio.shopping.app.ShoppinCart;
import sample.stratio.shopping.domain.Basket;
import sample.stratio.shopping.domain.Goods;
import sample.stratio.shopping.domain.Total;
import sample.stratio.shopping.services.ConvertService;
import sample.stratio.shopping.services.ConvertServiceImpl;
import sample.stratio.shopping.services.OffersService;
import sample.stratio.shopping.services.OffersServiceImpl;

public class ShoppingTest {
	
	private ShoppinCart shoppinCart;
	private ConvertService convertService;
	private OffersService offersService;
	
	@Before
	public void before() {
		convertService = new ConvertServiceImpl();
		offersService = new OffersServiceImpl();
		offersService.setConvertService(convertService);
		
		shoppinCart = new ShoppinCart();
		shoppinCart.setOffersService(offersService);
		shoppinCart.setConvertService(convertService);
	}
	
	@Test
	public void basketEmpty() {
		Basket basket = new Basket(new Goods[] {}) ;
		
		Total total =  shoppinCart.getTotal(basket);
		
		assertNotNull(total);
		assertFalse( total.hasOffers() );
		assertEquals(0, total.getTotal() ,0);
	}
	
	@Test
	public void basketCheckApplesOffer() {
		Basket basket = new Basket(new Goods[]{ Goods.Apples });
		
		Total total =  shoppinCart.getTotal(basket);
		
		assertNotNull(total);
		assertTrue( total.hasOffers() );
		double euroPrice1 = applyPercentage(getEuroPrice(Goods.Apples), 0.10d);
		assertEquals(euroPrice1 , total.getTotal() ,0);
	}
	
	@Test
	public void basketCheckBreadOffer() {
		Basket basket = new Basket(new Goods[]{ Goods.Soup, Goods.Soup, Goods.Bread });
		
		Total total =  shoppinCart.getTotal(basket);
		
		assertNotNull(total);
		assertTrue( total.hasOffers() );
		assertEquals(1.89d , total.getTotal() ,0.005d);
	}

	@Test
	public void basketOneItemNoOffers() {
		Basket basket = new Basket(  new Goods[] {Goods.Milk});
		
		Total total =  shoppinCart.getTotal(basket);
		
		assertNotNull(total);
		assertFalse( total.hasOffers() );
		double euroPrice = getEuroPrice(Goods.Milk);
		assertEquals(euroPrice, total.getTotal() ,0);
	}
	
	@Test
	public void basketManyItemsNoOffers() {
		Basket basket = new Basket(new Goods[]{ Goods.Milk , Goods.Peanuts});
		
		Total total =  shoppinCart.getTotal(basket);
		
		assertNotNull(total);
		assertFalse( total.hasOffers() );
		double euroPrice1 = getEuroPrice(Goods.Milk);
		double euroPrice2 = getEuroPrice(Goods.Peanuts);
		assertEquals(euroPrice1 + euroPrice2, total.getTotal() ,0);
	}
	
	@Test
	public void basketManyItemsWithOffers() {
		Basket basket = new Basket(new Goods[]{ Goods.Apples, Goods.Milk, Goods.Bread });
		
		Total total =  shoppinCart.getTotal(basket);
		
		assertNotNull(total);
		assertTrue( total.hasOffers() );
		assertEquals(3.33d , total.getTotal() ,0.005d);
	}
	
	@Test
	public void basketTwoBagsOfApples() {
		Basket basket = new Basket(new Goods[]{ Goods.Apples, Goods.Apples });
		
		Total total =  shoppinCart.getTotal(basket);
		
		assertNotNull(total);
		assertTrue( total.hasOffers() );
		double euroPrice1 = applyPercentage(getEuroPrice(Goods.Apples)*2, 0.10d);
		assertEquals(euroPrice1 , total.getTotal() ,0);
	}

	private double applyPercentage(double euroPrice, double percentaje) {
		double x = euroPrice * percentaje;
		return euroPrice - x;
	}

	private double getEuroPrice(Goods goods) {
		return convertService.toEur(goods.getPrice(), goods.getCurrency());
	}
}
