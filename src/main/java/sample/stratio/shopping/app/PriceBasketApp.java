package sample.stratio.shopping.app;

import sample.stratio.shopping.domain.Basket;
import sample.stratio.shopping.domain.Total;
import sample.stratio.shopping.services.ConvertService;
import sample.stratio.shopping.services.ConvertServiceImpl;
import sample.stratio.shopping.services.OffersService;
import sample.stratio.shopping.services.OffersServiceImpl;
import sample.stratio.shopping.services.PrintService;
import sample.stratio.shopping.services.PrintServiceImpl;


public class PriceBasketApp 
{
    public static void main( String[] args )
    {
    	try {
            PrintService printService = new PrintServiceImpl();
            ShoppinCart shoppingCart = createShoppingCart();
            Basket basket = new Basket(args);
            Total total = shoppingCart.getTotal(basket);
            String myString = printService.print(total);
            System.out.println(myString);
		} catch (Exception e) {
			System.err.print(e);
			System.exit(-1);
		}
    }

	private static ShoppinCart createShoppingCart() {
		ConvertService convertService = new ConvertServiceImpl();
		OffersService offersService = new OffersServiceImpl();
		offersService.setConvertService(convertService);
		ShoppinCart shoppinCart = new ShoppinCart();
		shoppinCart.setOffersService(offersService);
		shoppinCart.setConvertService(convertService);
		return shoppinCart ;
	}
}
