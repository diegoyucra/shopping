package sample.stratio.shopping.app;

import java.util.Collection;

import sample.stratio.shopping.domain.Basket;
import sample.stratio.shopping.domain.BasketItem;
import sample.stratio.shopping.domain.Goods;
import sample.stratio.shopping.domain.Offers;
import sample.stratio.shopping.domain.Total;
import sample.stratio.shopping.services.ConvertService;
import sample.stratio.shopping.services.OffersService;

public class ShoppinCart {
	private ConvertService convertService;
	private OffersService offersService;
	
	public void setConvertService(ConvertService myConvertService) {
		convertService = myConvertService;
	}
	public void setOffersService(OffersService myoffersService) {
		this.offersService = myoffersService;
	}

	public Total getTotal(Basket basket) {
		assert basket != null;
		Total total =  new Total();
		double sum = getSubTotal(basket.getItems());
		Offers offers = offersService.getOffers(basket);

		total.setSubTotal(sum);
		total.setOffers(offers);
		return total;
	}
	private double getSubTotal(Collection<BasketItem> list) {
		double sum = 0;
		for (BasketItem basketItem : list) {
			Goods goods = basketItem.getGoods();
			double price = basketItem.getTotalPrice();
			sum += convertService.toEur( price, goods.getCurrency());
		}
		return sum;
	}
}
