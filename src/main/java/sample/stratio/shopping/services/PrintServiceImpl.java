package sample.stratio.shopping.services;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.Locale;

import sample.stratio.shopping.domain.Offer;
import sample.stratio.shopping.domain.Offers;
import sample.stratio.shopping.domain.Total;

public class PrintServiceImpl implements PrintService {
	final static DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.US);
	final static DecimalFormat df = new DecimalFormat("#0.00",otherSymbols);
	final static String EOL = "\r\n";
	
	final static String SUB_TOTAL_DISCOUNT_STRING= "Subtotal:{0} €"+ EOL;
	final static String SUB_TOTAL_STRING = "Subtotal:{0} € (No offers available)"+ EOL;
	final static String TOTAL_STRING =  "Total:{0} €";
	final static String DISCOUNT_STRING = "{0} {1}% off: -{2} €"+EOL;
	
	public String print(Total total) {
		StringBuilder result = new StringBuilder ();
		result.append(printSubTotal(total))
		.append(printDiscount(total.getOffers()))
		.append(printTotal(total));
		return result.toString();
	}

	private String printDiscount(Offers offers) {
		if ( offers.getOffers().isEmpty()) return "";
		StringBuilder result = new StringBuilder ();
		Collection<Offer> myOffers = offers.getOffers();
		for (Offer offer : myOffers) {
			result.append( MessageFormat.format(DISCOUNT_STRING,
					offer.getGoods().toString(), 
					offer.getPercentage(), 
					df.format(offer.getDiscount()) 
			));

		}
		return result.toString();
	}

	private String printTotal(Total total) {
		return MessageFormat.format( TOTAL_STRING , df.format(total.getTotal()));
	}

	private String printSubTotal(Total total) {
		String formatString = total.hasOffers() ? SUB_TOTAL_DISCOUNT_STRING: SUB_TOTAL_STRING;
		return MessageFormat.format(formatString, df.format(total.getSubTotal()) );
	}

}
