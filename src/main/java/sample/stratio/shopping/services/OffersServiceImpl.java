package sample.stratio.shopping.services;

import sample.stratio.shopping.domain.Basket;
import sample.stratio.shopping.domain.BasketItem;
import sample.stratio.shopping.domain.Goods;
import sample.stratio.shopping.domain.Offers;

public class OffersServiceImpl implements OffersService {
	private ConvertService convertService;

	public Offers getOffers(Basket basket) {
		Offers offers = new Offers();
		offers = checkOfferApples(basket, offers);
		offers = checkOfferBread(basket, offers);
		return offers;
	}

	private Offers checkOfferApples(Basket basket, Offers offers) {
		if ( basket.contains(Goods.Apples) ) {
			BasketItem item = basket.get(Goods.Apples);
			double discount = getDicount( item, 0.1 );
			offers.addOffer(Goods.Apples, 10 , discount);
		}
		return offers;
	}

	private Offers checkOfferBread(Basket basket, Offers offers) {
		if ( basket.contains(Goods.Bread)) {
			if ( basket.contains( Goods.Soup) && 
					basket.get(Goods.Soup).getNro() >=2 ) {
				BasketItem item = basket.get(Goods.Bread);
				double discount =  getDicount( item, 0.5 );
				offers.addOffer(Goods.Bread, 50, discount );	
			}
		}
		return offers;
	}
	
	public void setConvertService(ConvertService convertService) {
		this.convertService = convertService;
	}

	private double getDicount(BasketItem item, double percentage) {
		double eurPrice = convertService.toEur(item.getGoods().getPrice(), item.getGoods().getCurrency());
		return (eurPrice * item.getNro()) * percentage;
	}

}
