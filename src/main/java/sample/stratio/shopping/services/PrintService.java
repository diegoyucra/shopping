package sample.stratio.shopping.services;

import sample.stratio.shopping.domain.Total;

public interface PrintService {
	
	public String print(Total total);
	
}
