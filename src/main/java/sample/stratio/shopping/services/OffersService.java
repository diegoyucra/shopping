package sample.stratio.shopping.services;

import sample.stratio.shopping.domain.Basket;
import sample.stratio.shopping.domain.Offers;

public interface OffersService {
	
	public Offers getOffers(Basket basket);

	public void setConvertService(ConvertService convertService);
	
}
