package sample.stratio.shopping.services;

import sample.stratio.shopping.domain.Currency;

public interface ConvertService {

	public double toEur(double pound, Currency currency);

}
