package sample.stratio.shopping.services;

import java.util.HashMap;
import java.util.Map;

import sample.stratio.shopping.domain.Currency;

public class ConvertServiceImpl implements ConvertService {
	public static final double EUR_TO_LIB = 0.9; 
	public static final double EUR_TO_PENNY = 90;
	
	private static final Map<Currency, Double> mapConv = new HashMap<Currency, Double>();
	
	static {
		mapConv.put( Currency.Eur, 1d);
		mapConv.put( Currency.Pound, EUR_TO_LIB);
		mapConv.put( Currency.Penny, EUR_TO_PENNY);
	}
	
	public double toEur(double pound, Currency currency) {
		assert mapConv.containsKey(currency);
		double valueByEur = mapConv.get(currency);
		return pound / valueByEur;
	}
}
