package sample.stratio.shopping.domain;

import java.util.ArrayList;
import java.util.List;

public class Offers {
	
	private List<Offer> offers = new ArrayList<Offer>();
	private double total = 0;
	
	public void addOffer(Goods goods, int percentage, double discount) {
		Offer newOffer = new Offer(goods, percentage, discount);
		total += discount;
		offers.add(newOffer);
	}
	public List<Offer> getOffers() {
		return offers;
	}
	public double getTotal() {
		return total;
	}
	
	
}
