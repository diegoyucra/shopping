package sample.stratio.shopping.domain;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Basket {
	private Map<Goods, BasketItem> itemAndUnit = new HashMap<Goods, BasketItem>();

	public Basket(String[] list) {
		for (String item : list) {
			addItem( Goods.valueOf(item) );
		}
	}

	public Basket(Goods[] list) {
		for (Goods goods : list) {
			addItem(goods);
		}
	}

	public void addItem(Goods goods) {
		BasketItem item = itemAndUnit.get(goods);
		if (item == null) {
			item = new BasketItem(goods);
			itemAndUnit.put(goods, item);
		}
		item.add();
	}

	public Collection<BasketItem> getItems() {
		return itemAndUnit.values();
	}

	public boolean contains(Goods goods) {
		return itemAndUnit.containsKey(goods);
	}

	public BasketItem get(Goods goods) {
		return itemAndUnit.get(goods);
	}
}
