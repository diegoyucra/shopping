package sample.stratio.shopping.domain;

public enum Goods {
	Soup(65d, Currency.Penny),
	Bread(80d, Currency.Penny),
	Apples(1d, Currency.Pound),
	Milk(1.30d , Currency.Pound),
	Peanuts(2d, Currency.Eur), 
	Banana(1.20d, Currency.Eur);

	private double price;
	private Currency currency;
	
	private Goods( double myprice, Currency myCurrency) {
		this.price = myprice;
		this.currency = myCurrency;
	}
	
	public double getPrice() {
		return price;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

}
