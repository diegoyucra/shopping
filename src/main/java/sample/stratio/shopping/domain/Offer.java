package sample.stratio.shopping.domain;

public class Offer {
	private Goods goods; 
	private int percentage;
	private double discount;
	

	Offer(Goods goods, int percentage, double discount) {
		this.goods = goods;
		this.percentage = percentage;
		this.discount = discount;
	}
	
	public Goods getGoods() {
		return goods;
	}

	public int getPercentage() {
		return percentage;
	}

	public double getDiscount() {
		return discount;
	}

}
