package sample.stratio.shopping.domain;

public class BasketItem {
	private Goods goods;
	private int nro = 0;
	
	public BasketItem(Goods goods) {
		this.goods = goods;
	}
	public Goods getGoods() {
		return goods;
	}
	public void add() {
		nro++;
	}
	public int getNro() {
		return nro;
	}
	public double getTotalPrice() {
		return goods.getPrice()*getNro();
	}
	
	
}
