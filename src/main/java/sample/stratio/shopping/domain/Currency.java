package sample.stratio.shopping.domain;

public enum Currency {
	Pound, 
	Eur, 
	Penny
}
