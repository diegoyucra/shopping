package sample.stratio.shopping.domain;

public class Total {

	
	private double subTotal;
	private Offers offers = new Offers();

	public boolean hasOffers() {
		return offers.getTotal() > 0;
	}

	public double getTotal() {
		return  subTotal - offers.getTotal();
	}

	public double getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(double subTotal) {
		this.subTotal = subTotal;
	}

	public Offers getOffers() {
		return offers;
	}

	public void setOffers(Offers offers) {
		this.offers = offers;
	}

		
}
